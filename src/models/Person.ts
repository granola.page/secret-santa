export class Person {
    constructor(readonly name: string, readonly phoneNumber: string) {}
}